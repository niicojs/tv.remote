const TV = require('./lib/tv');

async function main() {
    const tv = new TV({
        ip: '192.168.0.69',
        port: 8001,
    });

    await tv.pair();

    await tv.connect();
    await tv.sendKey('KEY_VOLUP');
    tv.close();
}

main().then(() => console.log('Done.'));

// KEY_POWEROFF,
// KEY_VOLUP