FROM node:latest

ENV NODE_ENV=production
ENV API_PORT=8081

WORKDIR /app
COPY package*.json /app/
RUN npm install --production
COPY . /app/

# RUN npm i -g pm2

EXPOSE 8081

USER node
CMD ["node", "server.js"]
# CMD ["pm2", "start", "processes.json", "--no-daemon"]
