const TV = require('./lib/tv');

async function main() {
    const tv = new TV({ ip: process.env.TV_IP || '192.168.0.69' });
    await tv.pair();
    await tv.connect();
    await tv.sendKey('KEY_VOLUP');
    tv.close();
}

main();
