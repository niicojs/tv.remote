const fastify = require('fastify')({
    logger: true,
});
const TV = require('./lib/tv');

fastify.addHook('preHandler', (request, reply, next) => {
    const token = request.query['token'];
    if (token !== process.env.AUTH_TOKEN) {
        reply.code(401).send('Unauthorized');
    } else {
        next();
    }
});

fastify.get('/ping', async (request, reply) => {
    fastify.log.info(`Ping.`);
    return 'pong';
});

fastify.get('/tv/on', async (request, reply) => {
    fastify.log.info(`Turning tv on.`);    
});

fastify.get('/tv/off', async (request, reply) => {
    fastify.log.info(`Turning tv off.`);

    const tv = new TV({ ip: process.env.TV_IP || '192.168.0.69' });
    await tv.pair();
    await tv.connect();
    await tv.sendKey('KEY_POWEROFF');
    tv.close();

    return 'ok';
});

fastify.get('/volume/:updown', async (request, reply) => {
    fastify.log.info(`Volume ${request.params.updown}.`);

    const tv = new TV({ ip: process.env.TV_IP || '192.168.0.69' });
    await tv.pair();
    await tv.connect();
    if (request.params.updown === 'up') {
        await tv.sendKey('KEY_VOLUP');
    } else {
        await tv.sendKey('KEY_VOLDOWN');
    }
    tv.close();

    return 'ok';
});

const start = async () => {
    try {
        const port = +process.env.API_PORT|| 8081;
        await fastify.listen(port, '::');
        fastify.log.info(`Ready`);
    } catch (err) {
        fastify.log.error(err);
        process.exit(1);
    }
};
start();
