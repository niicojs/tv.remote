const WebSocket = require('ws');
const fs = require('mz/fs');
const got = require('got');

const Pairing = require('./pairing');
const Encryption = require('./Encryption/');

class TV {
    constructor(config) {
        this.ip       = config.ip       || '192.168.0.69';
        this.userId   = config.userId   || '654321';
        this.appId    = config.appId    || 'com.samsung.companion';
        this.socket   = null;
        this.identity = null;
        this.duid     = null;
    }

    async pair() {
        if (fs.existsSync('pairing.json')) {
            const json = await fs.readFile('pairing.json', 'utf8');
            this.identity = JSON.parse(json);
        } else {
            this.identity = await new Pairing(this).pair();
            await fs.writeFile('pairing.json', JSON.stringify(this.identity), 'utf8');
        }
        return this.identity;
    }

    async connect() {
        const result = await got(`http://${this.ip}:8000/socket.io/1`);
        const handshake = result.body.substring(0, result.body.indexOf(':'));
        return new Promise((resolve, reject) => {
            try {
                this.socket = new WebSocket(`ws://${this.ip}:8000/socket.io/1/websocket/${handshake}`);

                this.socket.on('message', data => {
                    if (data === '1::') {
                        this.wsinit();
                    } else if (data === '1::/com.samsung.companion') {
                        // nothing
                    } else if (data === '2::') {
                        console.log('ws keep alive');
                        this.socket.send('2::');
                    } else if (data.startsWith('5::/com.samsung.companion:')) {
                        const event = JSON.parse(data.slice('5::/com.samsung.companion:'.length));
                        if (event.name === 'receiveCommon') {
                            const decrypted = JSON.parse(Encryption.decryptData(this.identity.aesKey, event.args));
                            if (decrypted.api === 'GetDUID') {
                                this.duid = decrypted.result;
                                // console.log('duid is ' + this.duid);
                                resolve(decrypted.result);
                            }
                        }
                    } else {
                        console.log(data);
                    }
                });

                this.socket.on('error', e => {
                    console.log(e);
                    reject(e);
                });
            } catch (e) {
                reject(e);
            }
        });
    }

    wsinit() {
        this.socket.send('1::/com.samsung.companion');
        this.send('registerPush', {
            eventType: 'EMP',
            plugin: 'SecondTV'
        });
        this.send('registerPush', {
            eventType: 'EMP',
            plugin: 'RemoteControl'
        });
        this.send('callCommon', {
            method: 'POST',
            body: {
                plugin: 'NNavi',
                api: 'GetDUID',
                version: '1.000'
            }
        });
    }

    send(name, data) {
        const encrypted = Encryption.encryptData(this.identity.aesKey, this.identity.sessionId, data);
        const payload = '5::/com.samsung.companion:' + JSON.stringify({
            name,
            args: [ encrypted ],
        });
        this.socket.send(payload);
    }

    sendKey(key) {
        this.send('callCommon', {
            method: 'POST',
            body: {
                plugin: 'RemoteControl',
                version: '1.000',
                api: 'SendRemoteKey',
                param1: this.duid,
                param2: 'Click',
                param3: key,
                param4: 'false'
            }
        });
    }

    close() {
        this.socket.close();
    }
}

module.exports = TV;
