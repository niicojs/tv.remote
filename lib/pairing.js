const got = require('got');
const readline = require('readline');

const Encryption = require('./Encryption/');

class Pairing {
    constructor(tv) {
        this.tv = tv;
    }

    async pair() {
        await this.getDeviceInfo();
        await this.showPin();
        const pin = await this.readPin();
        const requestId = await this.confirmPin(pin);
        const identity = await this.acknowledge(requestId);
        await this.hidePinOnTv();
        return identity;
    }

    async getDeviceInfo() {
        const result = await got(`http://${this.tv.ip}:8001/api/v2/`, { json: true });
        this.device = result.body;
        console.log(`Device ${this.device.name}, id ${this.device.id}`);
        // this.device.id = undefined;
    }

    async showPin() {
        console.log('Step 0, show pin on TV...');
        let result = await got.post(`http://${this.tv.ip}:8080/ws/apps/CloudPINPage`);
        console.log('  result: ' + result.body);

        result = await got(`http://${this.tv.ip}:8080/ws/pairing?step=0&app_id=${this.tv.appId}&device_id=${this.device.id}&type=1`);
        console.log('  step0: ' + result.statusCode);
    }

    async readPin() {
        return new Promise((resolve, reject) => {
            try {
                const rl = readline.createInterface({
                    input: process.stdin,
                    output: process.stdout
                });
                
                rl.question('enter pin displayed on TV> ', (answer) => {
                    rl.close();
                    resolve(answer);
                });
            } catch (e) {
                reject(e);
            }
        });
    }

    async confirmPin(pin) {
        console.log('Step 1, confirm pin...');

        const hello = Encryption.generateServerHello(this.tv.userId, pin);
        console.log('  ServerHello: ' + hello);
        
        const result = await got.post(`http://${this.tv.ip}:8080/ws/pairing?step=1&app_id=${this.tv.appId}&device_id=${this.device.id}`, {
            json: true,
            body: {
                auth_data: {
                    auth_type: 'SPC',
                    GeneratorServerHello: hello,
                },
            },
        });

        const authData = JSON.parse(result.body.auth_data);        
        const check = Encryption.parseClientHello(authData.GeneratorClientHello);
        if (check < 0) {
            throw new Error('Error verifying pin!');
        }

        return authData.request_id;
    }

    async acknowledge(requestId) {
        console.log('Step 2, acknowledge...');
        const serverAck = Encryption.generateServerAcknowledge();
        const result = await got.post(`http://${this.tv.ip}:8080/ws/pairing?step=2&app_id=${this.tv.appId}&device_id=${this.device.id}`, {
            json: true,
            body: {
                auth_data: {
                    auth_type: 'SPC',
                    request_id: requestId,
                    ServerAckMsg: serverAck,
                },
            },
        });

        const authData = JSON.parse(result.body.auth_data);
        Encryption.parseClientAcknowledge(authData.ClientAckMsg);

        return {
            sessionId: authData.session_id,
            aesKey: Encryption.getKey(),
        };
    }

    async hidePinOnTv() {
         await got.delete(`http://${this.tv.ip}:8080/ws/apps/CloudPINPage/run`);
    }
}

module.exports = Pairing;
