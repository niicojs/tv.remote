const got = require('got');
const tunnel = require('tunnel');

const mygot = function(url, options = {}) {
    options.agent = tunnel.httpOverHttp({
        proxy: {
            host: 'localhost',
            port: 8888,
        },
    });
    return realgot(url, options);
};
mygot.post = function(url, options = {}) {
    options.method = 'POST';
    return got(url, options);
};
mygot.delete = function(url, options = {}) {
    options.method = 'DELETE';
    return got(url, options);
};

module.exports = mygot;
