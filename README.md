```
docker build -t tv-remote .
docker run -it --rm --name remote -p 8081:8081 tv-remote
docker ps
docker kill remote
```

```
docker -H 0.0.0.0:2375 build -t tv-remote .
docker -H 0.0.0.0:2375 run -it --rm --name remote -p 8081:8081 -e AUTH_TOKEN=secret tv-remote
docker -H 0.0.0.0:2375 ps
docker -H 0.0.0.0:2375 kill remote
```
